import React, { useState } from 'react';
import { Grid } from '@looker/components';
import styled from 'styled-components'
import { EmbedDashboard } from './components/Embed/EmbedDashboard';
import { LookerEmbedDashboard } from '@looker/embed-sdk';
import { EditorSidebar } from './EditorSidebar';

export function DynamicTester() {
  const [dashboard, setDashboard] = useState<LookerEmbedDashboard>()
  const [options, setOptions] = useState<any | undefined>();

  return (
        <EmbedDashboard
          {...{options, dashboard, setOptions, setDashboard}}
        />
  );
}

// @ts-ignore
const StyledGrid = styled(Grid)`
  grid-template-columns: 400px auto;
`