import React, { useCallback, useContext, useState, useEffect } from 'react'
import { LookerEmbedSDK, LookerEmbedDashboard } from '@looker/embed-sdk'
import {
  ExtensionContext,
  ExtensionContextData,
} from '@looker/extension-sdk-react'
import styled from "styled-components"
import { useParams } from 'react-router-dom'
import { ExtensionButton } from '../ExtensionButton'
import {
  ComponentsProvider,
  FieldCheckboxGroup,
  SelectMulti,
  FieldSelectMulti,
  Heading, Box, ButtonOutline,
  Prompt, Button, Select, theme, 
  ButtonToggle, ButtonItem
} from "@looker/components";
import { useHistory } from 'react-router-dom';
import "./dashboard.css"

export const EmbedDashboard = ({ options, dashboard, setOptions, setDashboard}: any) => {
  const offerMeasures = [
  { label: "GC/R/D", value: "drt_agg.gc_r_d"},
  { label: "Total Guest Counts", value: "drt_agg.total_guest_counts"},
  { label: "Active Digital Customer Count", value: "drt_agg.active_digl_cust_count"},
  { label: "Average Check", value: "drt_agg.average_check"},
  { label: "Average Gross Margin", value: "drt_agg.average_gross_margin"},
  { label: "Total Gross Margins", value: "drt_agg.total_gross_margins" },
  { label: "Total Food Paper Costs", value: "drt_agg.total_food_paper_costs" },
  { label: "Average Gross Margin Percentage", value: "drt_agg.average_gross_margin_percentage"},
  { label: "Average Promo Costs", value: "drt_agg.average_promo_costs" },
  { label: "Average Food Paper Promo Costs", value: "drt_agg.average_food_paper_promo_costs"},
  { label: "Average Food Paper Costs", value: "drt_agg.average_food_paper_costs"},
  { label: "Total Promo Cost", value: "drt_agg.total_promo_cost" },
  { label: "Total Food Paper Promo Costs", value: "drt_agg.total_food_paper_promo_costs" },
  { label: "Total Net Sales", value: "drt_agg.total_net_sales"}
  ]
  const digitalMeasures = [
  { label: "GC/R/D", value: "GC/R/D", default: "True"},
  { label: "Total Guest Counts", value: "Total Guest Counts", default: "True"},
  { label: "Active Digital Customer Count", value: "Active Digital Customer Count", default: "True"},
  { label: "Average Check", value: "Average Check", default: "True"},
  { label: "Average Gross Margin", value: "Average Gross Margin", default: "True"},
  { label: "Average Gross Margin %", value: "Average Gross Margin %"},
  { label: "Average Promo Costs", value: "Average Promo Costs" },
  { label: "Average F&P Costs", value: "Average F&P Costs"},
  { label: "Average F&P + Promo Costs", value: "Average F&P + Promo Costs"},
  { label: "Total Gross Margin", value: "Total Gross Margin"},
  { label: "Total Promo Costs", value: "Total Promo Costs" },
  { label: "Total F&P Costs", value: "Total F&P Costs"},
  { label: "Total F&P + Promo Costs", value: "Total F&P + Promo Costs" },
  { label: "Total Net Revenue", value: "Total Net Revenue"}
  ]
  const storeMeasures = [
    { label: "GC/R/D", value: "drt_agg.gc_r_d", default: "True"},
    { label: "Total Guest Counts", value: "drt_agg.total_guest_counts", default: "True"},
    { label: "Active Digital Customer Count", value: "drt_agg.active_digl_cust_count"},
    { label: "Average Check", value: "drt_agg.average_check", default: "True"},
    { label: "Average Gross Margin", value: "drt_agg.average_gross_margin"},
    { label: "Total Gross Margins", value: "drt_agg.total_gross_margins" },
    { label: "Total Food Paper Costs", value: "drt_agg.total_food_paper_costs" },
    { label: "Average Gross Margin Percentage", value: "drt_agg.average_gross_margin_percentage"},
    { label: "Average Promo Costs", value: "drt_agg.average_promo_costs" },
    { label: "Average Food Paper Promo Costs", value: "drt_agg.average_food_paper_promo_costs"},
    { label: "Average Food Paper Costs", value: "drt_agg.average_food_paper_costs"},
    { label: "Total Promo Cost", value: "drt_agg.total_promo_cost" },
    { label: "Total Food Paper Promo Costs", value: "drt_agg.total_food_paper_promo_costs" },
    { label: "Total Net Sales", value: "drt_agg.total_net_sales"}
  ]

  const tabsMapper : { [key: string]: string } = {
    'id_10' : 'Digital Performance',
    'id_11' : 'Offer Performance',
    'id_24' : 'Store Performance'
  };
  const alwaysHidden = { label: "Offer Num", value: "top_offers.offer_num" }
  const storePerfHidden = { label: "Store Rnk", value: "store_rank.store_rnk" }
  const extensionContext = useContext<ExtensionContextData>(ExtensionContext)
  const { dashboard_id } = useParams();
  console.log("dashboard_id: " + dashboard_id)
  let history = useHistory();
  const [measures, setMeasures] = useState([{ "label": "Loading", "value": "loading" }]);
  const [selectedMeasures, setSelectedMeasures] = useState(["loading"]);
  const [filters, setFilters] = useState({});
  const [title, setTitle] = useState("");
  const [isOffer, setIsOffer] = useState(dashboard_id == 11);
  const [currentTab, setCurrentTab] = useState<string>(tabsMapper[`id_${dashboard_id}`]); //to change
  const [savedQueries, setSavedQueries] = useState<any[]>([]);
  const [savedUser, setSavedUser] = useState<any>()
  const [dashboardQueries, setDashboardQueries] = useState<any[]>([]);
  const sdk = extensionContext.core40SDK

  //const myTheme = {
  //  ...theme,
  //  yellow: 'FFBC0D',
  //  red: 'DB00007',
  //  lightgrey: '80808D',
  //  darkgrey: '53565A',
  //}

  const ThemedButton = styled(ExtensionButton)`
    background-color: #ffffff;
    color: #80808D;

    &:hover,
    &:focus {
      background-color: #FFBC0D;
      color: #53565A;
      border-color: #FFBC0D !important;
    }
  `

  const ThemedButtonItem = styled(ButtonItem)`
    background-color: #ffffff;
    color: #80808D;
  `

  const ThemedPrompt = styled(Prompt)`
    background-color: #ffffff;
    color: #80808D;

    &:hover,
    &:focus {
      background-color: #FFBC0D;
      color: #53565A;
    }
  `

  const ThemedSelectMulti = styled(SelectMulti)`
    background-color: #ffffff;
    color: #80808D;

    &:hover,
    &:focus {
      background-color: #FFBC0D;
      color: #53565A;
    }
  `

  useEffect(() => {
    const setUp = async () => {
      //get user data
      const me = await sdk.ok(sdk.me())
      setSavedUser(me)
      console.log(me)

      //get user attribute "saved_filters" (id 15) and parse into savedQueries
      var queryJson = await sdk.ok(sdk.user_attribute_user_values({
        user_id: me.id!,
        fields: "value"
      }))
      var savedFilters = queryJson[14].value
      if (savedFilters != undefined && savedFilters.length > 0) {
        var parsed = JSON.parse(savedFilters)
        console.log(parsed)
        setSavedQueries(parsed)
      }
    }
    setUp()
  }, [])

  useEffect(() => {
    //update dashboardQueries with only the dashboard specific queries loaded from above
    var filteredQueries = savedQueries.filter((savedQuery:any) => savedQuery.value.substring(0, 2) == dashboard_id)
    console.log(filteredQueries)
    setDashboardQueries(filteredQueries)
  }, [savedQueries, dashboard_id])

  useEffect(() => {
    if (measures[0].label != "Loading" && !isOffer) {
      changeOptions(selectedMeasures)
    }
  }, [measures])

  const changeQuery = (event: any) => {
      var filtered = savedQueries.filter((query:any) => event == query.value)
      .map((query:any) => query.filters)[0]
      console.log(filtered)
      setFilters(filtered)
      dashboard.updateFilters(filtered)
  }

  const updateDigitalOptions = (options: any, event: any) => {
      var opt = options
      var layouts = measures
      .filter((measure:any) => event.includes(measure.value))
      .map((measure:any) => measure.layout)
      console.log(layouts)
      var col = true
      for (const layout of layouts) {
        if (col) {
          layout.column = 0
          col = false
        } else {
          layout.column = 12
          col = true
        }
      }
      console.log(layouts)
      opt.layouts[0].dashboard_layout_components = layouts
      return opt
  }

  const changeOptions = (event: any) => {
    console.log(event)
    if (currentTab === tabsMapper.id_10) {
      var opt = updateDigitalOptions(options, event)
      dashboard.setOptions(opt)
      setSelectedMeasures(measures
      .filter((measure:any) => event.includes(measure.value))
      .map((measure:any) => measure.value))
    } else if (currentTab === tabsMapper.id_11) {
      var opt = options
      var layouts = measures
      .filter((measure:any) => !event.includes(measure.value))
      .map((measure:any) => measure.value)
      console.log(layouts)
      layouts.push(alwaysHidden.value)
      opt.elements["36"].vis_config.hidden_fields = layouts
      dashboard.setOptions(opt)
      setSelectedMeasures(measures
      .filter((measure:any) => event.includes(measure.value))
      .map((measure:any) => measure.value))
    } else {
      var opt = options
      var layouts = measures
      .filter((measure:any) => !event.includes(measure.value))
      .map((measure:any) => measure.value)
      console.log(layouts)
      layouts.push(storePerfHidden.value)
      opt.elements["65"].vis_config.hidden_fields = layouts
      dashboard.setOptions(opt)
      setSelectedMeasures(measures
      .filter((measure:any) => event.includes(measure.value))
      .map((measure:any) => measure.value))
    }
  }

  const updateDashboard = (obj: any) => {
    dashboard.setOptions(obj)
  }

  const setupDashboard = (dashboard: LookerEmbedDashboard) => {
    setDashboard(dashboard)
  }

  const dashboardLoaded = (event: any) => {
    if (event?.dashboard?.options) {
      setOptions(event.dashboard.options);
      console.log(event.dashboard.options,"hi", event.dashboard)
      if (currentTab == tabsMapper.id_10) {
        var keys = Object.keys(event.dashboard.options["elements"])
        var elements = event.dashboard.options.elements
        var layout = event.dashboard.options.layouts[0].dashboard_layout_components
        var ids = layout.map((layout : any) => {
          var id = layout["dashboard_element_id"]
          var ret = { "label": elements[id]["title"], "value": elements[id]["title"], "layout": layout }
          return ret
        })
        var sortedIds = []
        var measureKey = tabsMapper.id_10 ? digitalMeasures : storeMeasures;
        console.log(measureKey);
        for (const m of measureKey) {
          for (const i of ids) {
            if (m.value == i.value) {
              if (m.default == "True") {
                i.default = "True"
              }
              sortedIds.push(i)
            }
          }
        }
        var selectedSortedIds = sortedIds.filter((id:any) => id["default"] == "True").map((id:any) => id.value)
        setSelectedMeasures(selectedSortedIds)
        setMeasures(sortedIds)
      } else if (currentTab == tabsMapper.id_11){
        console.log(offerMeasures);
        var selected = offerMeasures
        .filter((offer:any) => 
          !event.dashboard.options.elements["36"].vis_config.hidden_fields.includes(offer.value))
        .map((offer:any) => offer.value)
        setSelectedMeasures(selected)
        setMeasures(offerMeasures)
      } else {
        var selected = storeMeasures
        .filter((offer:any) => 
          !event.dashboard.options.elements["65"].vis_config.hidden_fields.includes(offer.value))
        .map((offer:any) => offer.value)
        setSelectedMeasures(selected)
        setMeasures(storeMeasures)
      }
    }
    if (event?.dashboard?.dashboard_filters) {
      setFilters(event?.dashboard?.dashboard_filters)
    }
    if (event?.dashboard?.title) {
      setTitle(event?.dashboard?.title)
    }
  }

  const filterChanged = (event: any) => {
    if (event?.dashboard?.dashboard_filters) {
      setFilters(event?.dashboard?.dashboard_filters)
    }
  }

  const embedCtrRef = useCallback(
    (el) => {
      const hostUrl = extensionContext?.extensionSDK?.lookerHostData?.hostUrl
      if (el && hostUrl) {
        el.innerHTML = ''
        LookerEmbedSDK.init(hostUrl)
        LookerEmbedSDK.createDashboardWithId(dashboard_id)
          .appendTo(el)
          .withNext()
          .on('dashboard:loaded', dashboardLoaded)
          .on('dashboard:filters:changed', filterChanged)
          .build()
          .connect()
          .then(setupDashboard)
          .catch((error: Error) => {
            console.error('Connection error', error)
          })
      }
    },
    [dashboard_id]
  )

  const toggleDashboard = (event: any) => {

    console.log(event, 'event');

    setCurrentTab(event);
    setTitle("")
    setMeasures([{ "label": "Loading", "value": "loading" }])
    setSelectedMeasures(["loading"])

    if(event === tabsMapper.id_10) {
      history.push(`/dashboards/10`);
    } else if(event === tabsMapper.id_11) {
      history.push(`/dashboards/11`);
    } else if(event === tabsMapper.id_24) {
      history.push(`/dashboards/24`);
    }
  }

  const downloadFile = () => {
    console.log(filters)
    var headers = '"' + Object.keys(filters).join('"&"').replace(/,/g, ", ").replace(/&/g, ",") + '"'
    var data = '"' + Object.values(filters).join('"&"').replace(/,/g, ", ").replace(/&/g, ",") + '"'
    var csv = headers + "\n" + data
    console.log(csv)
    const element = document.createElement("a")
    const file = new Blob([csv], {type: "text/plain"})
    element.href = window.URL.createObjectURL(file)
    element.download = "Template_" + title.replace(/ /g, "_") + ".csv"
    document.body.appendChild(element)
    element.click()
    element.remove()
  }

  const hiddenInput = React.useRef<any>(null);

  const clickInput = (event: any) => {
    hiddenInput!.current!.click()
  }

  const uploadFile = (event: any) => {
    if (event?.target?.files[0]) {
      const file = event.target.files[0]
      var reader = new FileReader()
      reader.readAsText(file)
      reader.onload = parseCsv
      reader.onerror = err
      event.target.value = null
    }
  }

  const parseCsv = (event: any) => {
    var csv = event.target.result
    var lines = csv.split(/\r\n|\n/);
    lines = lines.map((line:any) => line.substring(1, line.length-1).split("\",\""))
    var header = lines[0]
    var values = lines[1]
    var csvJson: {[index: string]:any} = {}
    for (var x = 0; x < header.length; x++) {
      csvJson[header[x]] = values[x]
    }
    console.log(csvJson)
    setFilters(csvJson)
    dashboard.updateFilters(csvJson)
  }

  const err = (event: any) => console.log("error" + event)

  return (
    <>
      {title &&
      <Box style={{backgroundColor: "#f6f8fa", height: "50px", display: "flex", position:"absolute", left:0, top:0, right:0,
      justifyContent: "center", alignItems: "center"}}>
        <img src='https://portal-dot-mcd-us-analytics.appspot.com/mcdonalds-png.png' alt="Golden Arches" style={{height:"50px"}} />
        <Heading as="h1">DRT</Heading>
        <label className=".TfJAx" style={{display: "inlineBlock", textAlign: "right", marginLeft:"1%", marginRight:".5%", marginBottom:"0px", maxHeight:"36px", width:"50px", fontSize: "0.75rem", color: "#53565A"}}>Select Measures</label>
        <SelectMulti
          label="Select Measures"
          disabled={!title}
          options={measures}
          values={selectedMeasures}
          defaultValues={selectedMeasures}
          placeholder="Search Measures"
          style={{marginLeft:"0%", marginRight:"1%", marginBottom:"0px", maxHeight:"36px", width:"500px",
          backgroundColor: "#ffffff !important", color:"#80808D !important"}}
          onChange={changeOptions}
        />
        <input 
            type="file" 
            accept=".csv" 
            ref={hiddenInput}
            onChange={uploadFile}
            style={{display:"none"}}
        />
        {isOffer != undefined &&
        <ButtonToggle 
        value={currentTab}
        onChange={toggleDashboard} style={{marginLeft:"1%", marginRight:"1%", flexWrap:"nowrap"}}>
          <ThemedButtonItem disabled={!title}>{tabsMapper.id_10}</ThemedButtonItem>
          <ThemedButtonItem disabled={!title}>{tabsMapper.id_11}</ThemedButtonItem>
          <ThemedButtonItem disabled={!title}>{tabsMapper.id_24}</ThemedButtonItem>
        </ButtonToggle>
        }
        <ThemedButton
              mt="small"
              variant="outline"
              onClick={downloadFile}
              disabled={!title}
              style={{marginLeft:"1%", marginRight:"1%"}}
            >
              Download Template
        </ThemedButton>
        <ThemedButton
              mt="small"
              variant="outline"
              onClick={clickInput}
              disabled={!title}
              style={{marginLeft:"1%", marginRight:"1%"}}
            >
              Upload Template
        </ThemedButton>
        {savedQueries && savedUser &&
        <>
        <Select
          placeholder="Saved Queries"
          options={dashboardQueries}
          value="Saved Queries"
          onChange={changeQuery}
          style={{marginLeft:"1%", marginRight:"1%", marginBottom:"0px", maxHeight:"36px", width:"500px"}}
        />
        <ThemedPrompt
          title={'Choose a name for the saved query'}
          inputLabel={'Query Name'}
          onCancel={(close) => {
            close()
          }}
          onSave={(value, close) => {
            var val = dashboard_id + value
            var queries = [...savedQueries]
            for (let x = 0; x < queries.length; x++) {
              if (queries[x].value == val) {
                queries.splice(x,1)
              }
            }
            queries.push({ label: value, value: val, filters: filters })
            setSavedQueries(queries)
            sdk.ok(sdk.set_user_attribute_user_value(savedUser.id, 15, { "value": JSON.stringify(queries) }))
            close()
          }}
        >
          {(open) => <ThemedButton onClick={open} 
          style={{marginLeft:"1%", marginRight:"1%"}}
          mt="small" variant="outline">Save</ThemedButton>}
        </ThemedPrompt>
        </>
        }
      </Box>
      }
      <EmbedContainer ref={embedCtrRef}/>
    </>

  )
}

export const EmbedContainer = styled.div`
  width: 100%;
  height: 100vh;
  & > iframe {
    width: 100%;
    height: 100%;
    padding-top: 50px;
  }
`