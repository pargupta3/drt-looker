application: drt {
  label: "DRT"
  file: "bundle.js"
  entitlements: {
    local_storage: yes
    use_embeds: yes
    navigation: yes
    use_form_submit: yes
    new_window: yes
    core_api_methods: ["all_dashboards","search_content_favorites", "search_dashboards", "me"]
  }
}